package com.springboot.prototype.controller;

import java.util.Date;
import java.util.List;
import java.util.Locale;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.springboot.prototype.dto.PrototypeDto;
import com.springboot.prototype.service.PrototypeService;
import com.springboot.prototype.util.MessageUtil;

@RestController
public class PrototypeController {
	
	@Autowired
	PrototypeService prototypeService;
	
	@Autowired
	MessageUtil messageUtil;
	
	@GetMapping("")
	public List<PrototypeDto> index() {
		PrototypeDto prototypeDto = new PrototypeDto();
		prototypeDto.setCreatedDate(new Date());
		prototypeDto.setType("CHARGGG");
		
		prototypeService.create(prototypeDto);
		
		prototypeService.deleteById(2L);
		
		prototypeService.updateById(3L);
			
		return prototypeService.findAll();
	}
	
	@GetMapping("/")
	public String getLocale() {
		return messageUtil.getMessage("title");
	}

}
