package com.springboot.prototype.service;

import java.util.List;

import com.springboot.prototype.dto.PrototypeDto;

public interface PrototypeService {
	
	List<PrototypeDto> findAll();
	
	void create(PrototypeDto prototypeDto);
	
	void deleteById(Long id);
	
	void updateById(Long id);

}
