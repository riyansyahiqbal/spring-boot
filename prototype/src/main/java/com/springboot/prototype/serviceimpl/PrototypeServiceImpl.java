package com.springboot.prototype.serviceimpl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.springboot.prototype.dao.PrototypeDao;
import com.springboot.prototype.dto.PrototypeDto;
import com.springboot.prototype.service.PrototypeService;

@Service
public class PrototypeServiceImpl implements PrototypeService{

	@Autowired
	private PrototypeDao prototypeDao;
	
	@Override
	public List<PrototypeDto> findAll() {
		return prototypeDao.findAll();
	}

	@Override
	public void create(PrototypeDto prototypeDto) {
		prototypeDao.create(prototypeDto);
	}

	@Override
	public void deleteById(Long id) {
		prototypeDao.deleteById(id);
	}

	@Override
	public void updateById(Long id) {
		prototypeDao.updateById(id);
	}

	

}
