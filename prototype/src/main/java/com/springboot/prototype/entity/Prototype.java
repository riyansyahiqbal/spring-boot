package com.springboot.prototype.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;

@Entity
@Table(name = "prototype", indexes = { @Index(name = "IDX_NFLOG", columnList = "PARTNER_CUSTOMER_ID,PARTNER_REFERENCE_ID,NETFLIX_TRANSACTION_ID") } )
@Getter @Setter
public class Prototype {
	
	@Id
	@Column(name = "ID")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long id;
	
	@Column(name = "TYPE", columnDefinition=" VARCHAR(25) NOT NULL COMMENT 'INIT_SUBSCRIBE / READ_CHARGE/ SEND_CHARGE'")
	private String type;
	
	@Column(name = "PARTNER_CUSTOMER_ID", length = 50)
	private String partnerCustomerId;
	
	@Column(name = "PARTNER_REFERENCE_ID", length = 50)
	private String partnerReferenceId;
	
	@Column(name = "NETFLIX_TRANSACTION_ID", length = 50)
	private String netflixTransactionId;

	@Column(name = "REQUEST", columnDefinition="JSON")
	private String request;
	
	@Column(name = "RESPONSE", columnDefinition="JSON")
	private String response;
	
	@Column(name = "RESPONSE_CODE", length=10)
	private String responseCode;

	@Column(name = "TRANSACTION_ID")
	private Long transactionId;
	
	@Column(name = "CREATED_DATE")
	private Date createdDate;
	
	@Column(name = "UPDATED_DATE")
	private Date updatedDate;
	
}
