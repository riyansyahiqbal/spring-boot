package com.springboot.prototype.dto;

import java.util.Date;

import lombok.Data;

@Data
public class PrototypeDto {
	private Long id;
	private String type;
	private String partnerCustomerId;
	private String partnerReferenceId;
	private String netflixTransactionId;
	private String request;
	private String response;
	private String responseCode;
	private Long transactionId;
	private Date createdDate;
	
}
