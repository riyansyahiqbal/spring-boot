package com.springboot.prototype.util;

import java.util.Locale;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

@Component
public class MessageUtil {
	
	private Logger log = LoggerFactory.getLogger(MessageUtil.class);
	
	@Autowired(required = false)
	private MessageSource messageSource;

	public MessageSource getMessageSource() {
		return messageSource;
	}

	public String getMessage(String code) {
		return this.getMessage(code, null);
	}

	public String getMessage(String code, Object...args) {
		return this.messageSource.getMessage(code, args, getRequestLanguage());
	}
	
	private Locale getRequestLanguage(){
    	Locale locale = null;
    	try {
    		if(RequestContextHolder.getRequestAttributes() == null){
    			return locale;
    		}
    		HttpServletRequest request =  ((ServletRequestAttributes)RequestContextHolder.getRequestAttributes()).getRequest();
    		locale = getDefaultLocale(request.getHeader("Accept-Language"));
		} catch (Exception e) {
			log.error("Exception Locale. Cause : ",e);
		}
    	return locale;
    }
	
	private Locale getDefaultLocale(String lang) {
		switch (lang) {
		case "in_ID":
			return new Locale("in", "ID");
		case "en_US":
			return Locale.US;
		}
		return null;
	}

}
