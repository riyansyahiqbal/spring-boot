package com.springboot.prototype.config;

import java.util.Locale;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.ReloadableResourceBundleMessageSource;

@Configuration
public class MessageResourceConfig {
	
	@Value("${locale.default}")
	private String defaultLocale;
	
	@Bean
    public MessageSource messageSource() {
        ReloadableResourceBundleMessageSource messageSource = new ReloadableResourceBundleMessageSource();
        messageSource.setBasename("classpath:messages/messages");
        messageSource.setDefaultEncoding("UTF-8");
        messageSource.setDefaultLocale(getDefaultLocale());
        return messageSource;
    }
	
	private Locale getDefaultLocale() {
		switch (defaultLocale) {
		case "ID":
			return new Locale("in", "ID");
		case "US":
			return Locale.US;
		default:
			return new Locale("in", "ID");
		}
	}

    
   
}
