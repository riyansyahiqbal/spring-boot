package com.springboot.prototype.dao;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;

import com.springboot.prototype.dto.PrototypeDto;

@Mapper
public interface PrototypeDao {

	List<PrototypeDto> findAll();

	void create(PrototypeDto prototypeDto);

	void deleteById(Long id);

	void updateById(Long id);

}
